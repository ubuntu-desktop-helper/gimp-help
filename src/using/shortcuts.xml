<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE sect1 PUBLIC "-//OASIS//DTD DocBook XML V4.3//EN"
                       "http://www.docbook.org/xml/4.3/docbookx.dtd">
<!--Version history
  2018-09-30 aprokoudine: update for 2.10 and Windows 10
  2015-12-21 j.h: fixed bug #744449
  2007-07-15 lexa: removed inappropriate step
  2005-11-01 Created by julien hardelin
-->
<sect1 id="gimp-concepts-shortcuts">
  <title>Creating Shortcuts to Menu Commands</title>

  <indexterm>
    <primary>Shortcuts</primary>
  </indexterm>
  <indexterm>
    <primary>Customize</primary>
    <secondary>Shortcuts</secondary>
  </indexterm>

  <para>
    Many functions which are accessible via the image menu have a default
    keyboard shortcut. You may want to create a new shortcut for a command
    that you use a lot and doesn't have one or, more rarely, edit an existing
    shortcut. There are two methods for doing this.
  </para>

  <procedure>
    <title>Using dynamic keyboard shortcuts</title>
    <step>
      <para>
        First, you have to activate this capability by checking the
        <guilabel>Use dynamic keyboard shortcuts</guilabel> option in
        the <guilabel>Interface</guilabel> item of the
        <link linkend="gimp-prefs-interface">Preferences</link> menu.  This
        option is usually not checked, to prevent accidental key presses
        from creating an unwanted shortcut.
      </para>
    </step>
    <step>
      <para>
        While you're doing that, also check the <guilabel>Save keyboard
        shortcuts on exit</guilabel> option so that your shortcut will
        be saved.
      </para>
    </step>
    <step>
      <para>
        To create a keyboard shortcut, simply place the mouse pointer on a
        command in the menu: it will then be highlighted. Be careful that the
        mouse pointer doesn't move and type a sequence of three keys, keeping
        the keys pressed. You will see this sequence appear on the right of
        the command.
      </para>
    </step>
    <step>
      <para>
        It is best to use the
        <keycombo>
          <keycap>Ctrl</keycap>
          <keycap>Alt</keycap>
          <keycap>Key</keycap>
        </keycombo>
        sequence for your custom shortcuts.
      </para>
    </step>
  </procedure>

  <procedure>
    <title>Using the Keyboard Shortcut Dialog</title>
    <step>
      <para>
        The <link
        linkend="gimp-help-keyboard-shortcuts">Keyboard Shortcuts Dialog</link>
        can be used to assign keyboard shortcuts. Not only for menu commands,
        but also for any other command and it does not need changing any
        preference.
      </para>
    </step>
  </procedure>

</sect1>
