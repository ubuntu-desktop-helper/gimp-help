msgid ""
msgstr ""
"Project-Id-Version: GIMP-Help 2.8.0\n"
"POT-Creation-Date: 2023-03-05 19:42+0000\n"
"PO-Revision-Date: 2012-06-01 12:23+0200\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: en_GB\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: src/menus/colors/auto/white-balance.xml:69(None)
#: src/menus/colors/auto/stretch-contrast.xml:120(None)
#: src/menus/colors/auto/stretch-contrast-hsv.xml:55(None)
#: src/menus/colors/auto/equalize.xml:54(None)
#: src/menus/colors/auto/color-enhance-legacy.xml:59(None)
msgid ""
"@@image: 'images/menus/colors/auto/alice.png'; "
"md5=a33d190d14dbff2cc22559afe586614b"
msgstr ""
"@@image: 'images/menus/colors/auto/alice.png'; "
"md5=a33d190d14dbff2cc22559afe586614b"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: src/menus/colors/auto/white-balance.xml:84(None)
msgid ""
"@@image: 'images/menus/colors/auto/white-balance.png'; "
"md5=047e4fec700babd6f8a2f9a3be6b8c6f"
msgstr ""
"@@image: 'images/menus/colors/auto/white-balance.png'; "
"md5=047e4fec700babd6f8a2f9a3be6b8c6f"

#: src/menus/colors/auto/white-balance.xml:10(title)
#: src/menus/colors/auto/white-balance.xml:17(primary)
msgid "White Balance"
msgstr "White Balance"

#: src/menus/colors/auto/white-balance.xml:13(primary)
#: src/menus/colors/auto/stretch-contrast.xml:14(primary)
#: src/menus/colors/auto/stretch-contrast-hsv.xml:13(primary)
#: src/menus/colors/auto/equalize.xml:12(primary)
#: src/menus/colors/auto/color-enhance.xml:9(primary)
#: src/menus/colors/auto/color-enhance-legacy.xml:12(primary)
msgid "Colors"
msgstr "Colors"

#: src/menus/colors/auto/white-balance.xml:14(secondary)
msgid "White balance"
msgstr "White balance"

#: src/menus/colors/auto/white-balance.xml:20(para)
#, fuzzy
msgid ""
"The <guimenuitem>White Balance</guimenuitem> command automatically adjusts "
"the colors of the active layer by stretching the Red, Green and Blue "
"channels separately. To do this, it discards pixel colors at each end of the "
"Red, Green and Blue histograms which are used by only 0.05% of the pixels in "
"the image and stretches the remaining range as much as possible. The result "
"is that pixel colors which occur very infrequently at the outer edges of the "
"histograms (perhaps bits of dust, etc.) do not negatively influence the "
"minimum and maximum values used for stretching the histograms, in comparison "
"with <link linkend=\"gimp-filter-stretch-contrast\">Stretch Contrast</link>. "
"Like <quote>Stretch Contrast</quote>, however, there may be hue shifts in "
"the resulting image."
msgstr ""
"The <guimenuitem>White Balance</guimenuitem> command automatically adjusts "
"the colors of the active layer by stretching the Red, Green and Blue "
"channels separately. To do this, it discards pixel colors at each end of the "
"Red, Green and Blue histograms which are used by only 0.05% of the pixels in "
"the image and stretches the remaining range as much as possible. The result "
"is that pixel colors which occur very infrequently at the outer edges of the "
"histograms (perhaps bits of dust, etc.) do not negatively influence the "
"minimum and maximum values used for stretching the histograms, in comparison "
"with <link linkend=\"gimp-filter-stretch-contrast\">Stretch Contrast</link>. "
"Like <quote>Stretch Contrast</quote>, however, there may be hue shifts in "
"the resulting image."

#: src/menus/colors/auto/white-balance.xml:34(para)
msgid ""
"This command suits images with poor white or black. Since it tends to create "
"pure white (and black), it may be useful e.g. to enhance photographs."
msgstr ""
"This command suits images with poor white or black. Since it tends to create "
"pure white (and black), it may be useful e.g. to enhance photographs."

#: src/menus/colors/auto/white-balance.xml:40(para)
msgid ""
"This command only works on RGB images. If the image is Grayscale or Indexed, "
"the menu entry is disabled."
msgstr ""

#: src/menus/colors/auto/white-balance.xml:47(title)
#: src/menus/colors/auto/stretch-contrast.xml:44(title)
#: src/menus/colors/auto/stretch-contrast-hsv.xml:33(title)
#: src/menus/colors/auto/equalize.xml:36(title)
msgid "Activate the Command"
msgstr "Activate the Command"

#: src/menus/colors/auto/white-balance.xml:50(para)
#, fuzzy
msgid ""
"You can access this command from the image menubar through "
"<menuchoice><guimenu>Colors</guimenu><guisubmenu>Auto</"
"guisubmenu><guimenuitem>White Balance</guimenuitem></menuchoice>."
msgstr ""
"You can access this command from the image menubar through "
"<menuchoice><guimenu>Colors</guimenu><guisubmenu>Auto</"
"guisubmenu><guimenuitem>White Balance</guimenuitem></menuchoice>."

#: src/menus/colors/auto/white-balance.xml:63(title)
msgid "<quote>White Balance</quote> example"
msgstr "<quote>White Balance</quote> example"

#: src/menus/colors/auto/white-balance.xml:65(title)
#: src/menus/colors/auto/stretch-contrast.xml:116(title)
#: src/menus/colors/auto/stretch-contrast-hsv.xml:51(title)
#: src/menus/colors/auto/equalize.xml:50(title)
#: src/menus/colors/auto/color-enhance-legacy.xml:55(title)
msgid "Original image"
msgstr "Original image"

#: src/menus/colors/auto/white-balance.xml:72(para)
msgid ""
"The active layer and its Red, Green and Blue histograms before <quote>White "
"Balance</quote>."
msgstr ""
"The active layer and its Red, Green and Blue histograms before <quote>White "
"Balance</quote>."

#: src/menus/colors/auto/white-balance.xml:80(title)
#: src/menus/colors/auto/stretch-contrast.xml:131(title)
#: src/menus/colors/auto/stretch-contrast-hsv.xml:66(title)
#: src/menus/colors/auto/equalize.xml:65(title)
msgid "Image after the command"
msgstr "Image after the command"

#: src/menus/colors/auto/white-balance.xml:87(para)
msgid ""
"The active layer and its Red, Green and Blue histograms after <quote>White "
"Balance</quote>. Poor white areas in the image became pure white."
msgstr ""
"The active layer and its Red, Green and Blue histograms after <quote>White "
"Balance</quote>. Poor white areas in the image became pure white."

#: src/menus/colors/auto/white-balance.xml:92(para)
#: src/menus/colors/auto/stretch-contrast.xml:145(para)
msgid ""
"Histogram stretching creates gaps between the pixel columns, giving it a "
"striped look."
msgstr ""
"Histogram stretching creates gaps between the pixel columns, giving it a "
"striped look."

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: src/menus/colors/auto/stretch-contrast.xml:66(None)
#, fuzzy
msgid ""
"@@image: 'images/menus/colors/auto/stretch_contrast-dialog.png'; "
"md5=02a986c28ff64cf864c3d60d4525b8fd"
msgstr ""
"@@image: 'images/menus/colors/auto/stretch-hsv.png'; "
"md5=fb016a380256c6bbf3a302281a432904"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: src/menus/colors/auto/stretch-contrast.xml:135(None)
msgid ""
"@@image: 'images/menus/colors/auto/c-stretch.png'; "
"md5=7af9a55590071580eef4731b51cef312"
msgstr ""
"@@image: 'images/menus/colors/auto/c-stretch.png'; "
"md5=7af9a55590071580eef4731b51cef312"

#: src/menus/colors/auto/stretch-contrast.xml:11(title)
#: src/menus/colors/auto/stretch-contrast.xml:18(primary)
msgid "Stretch Contrast"
msgstr "Stretch Contrast"

#: src/menus/colors/auto/stretch-contrast.xml:15(secondary)
msgid "Stretch contrast"
msgstr "Stretch contrast"

#: src/menus/colors/auto/stretch-contrast.xml:21(primary)
msgid "Contrast"
msgstr "Contrast"

#: src/menus/colors/auto/stretch-contrast.xml:24(para)
#, fuzzy
msgid ""
"The <guimenuitem>Stretch Contrast</guimenuitem> command automatically "
"stretches the histogram values in the active layer. For each channel of the "
"active layer, it finds the minimum and maximum values and uses them to "
"stretch the Red, Green and Blue histograms to the full contrast range. The "
"bright colors become brighter and the dark colors become darker, which "
"increases the contrast. <quote>Stretch Contrast</quote> works on layers of "
"RGB, Grayscale and Indexed images. Use <quote>Stretch Contrast</quote> only "
"if you want to remove an undesirable color tint from an image which should "
"contain pure white and pure black."
msgstr ""
"The <guimenuitem>Stretch Contrast</guimenuitem> command automatically "
"stretches the histogram values in the active layer. For each channel of the "
"active layer, it finds the minimum and maximum values and uses them to "
"stretch the Red, Green and Blue histograms to the full contrast range. The "
"bright colors become brighter and the dark colors become darker, which "
"increases the contrast. This command produces a somewhat similar effect to "
"the <link linkend=\"plug-in-normalize\">Normalize</link> command, except "
"that it works on each color channel of the layer individually. This usually "
"leads to color shifts in the image, so it may not produce the desired "
"result. <quote>Stretch Contrast</quote> works on layers of RGB, Grayscale "
"and Indexed images. Use <quote>Stretch Contrast</quote> only if you want to "
"remove an undesirable color tint from an image which should contain pure "
"white and pure black."

#: src/menus/colors/auto/stretch-contrast.xml:36(para)
msgid ""
"This command is also similar to the <link linkend=\"gimp-layer-white-"
"balance\">Color Balance</link> command, but it does not reject any of the "
"very dark or very bright pixels, so the white might be impure."
msgstr ""
"This command is also similar to the <link linkend=\"gimp-layer-white-"
"balance\">Color Balance</link> command, but it does not reject any of the "
"very dark or very bright pixels, so the white might be impure."

#: src/menus/colors/auto/stretch-contrast.xml:47(para)
#, fuzzy
msgid ""
"This command can be accessed from an image menubar as "
"<menuchoice><guimenu>Colors</guimenu><guisubmenu>Auto</"
"guisubmenu><guimenuitem>Stretch Contrast</guimenuitem></menuchoice>."
msgstr ""
"This command can be accessed from an image menubar as "
"<menuchoice><guimenu>Colors</guimenu><guisubmenu>Auto</"
"guisubmenu><guimenuitem>Stretch Contrast</guimenuitem></menuchoice>."

#: src/menus/colors/auto/stretch-contrast.xml:60(title)
msgid "Options"
msgstr ""

#: src/menus/colors/auto/stretch-contrast.xml:62(title)
#, fuzzy
msgid "<quote>Stretch Contrast</quote> settings"
msgstr "<quote>Stretch Contrast</quote> Example"

#: src/menus/colors/auto/stretch-contrast.xml:72(term)
msgid "Presets"
msgstr ""

#: src/menus/colors/auto/stretch-contrast.xml:74(para)
msgid ""
"<quote>Presets</quote> are a common feature for several Colors commands. You "
"can find its description in <xref linkend=\"colors-common-features\"/>."
msgstr ""

#: src/menus/colors/auto/stretch-contrast.xml:83(term)
#, fuzzy
msgid "Keep Colors"
msgstr "Colors"

#: src/menus/colors/auto/stretch-contrast.xml:85(para)
msgid "Impact each color channel with the same amount."
msgstr ""

#: src/menus/colors/auto/stretch-contrast.xml:92(term)
msgid "Non-Linear Components"
msgstr ""

#: src/menus/colors/auto/stretch-contrast.xml:94(para)
msgid ""
"When set, this option operates on gamma corrected values instead of linear "
"RGB, acting like the old Normalize filter."
msgstr ""

#: src/menus/colors/auto/stretch-contrast.xml:102(term)
msgid "Blending Options, Preview and Split view"
msgstr ""

#: src/menus/colors/auto/stretch-contrast.xml:104(para)
msgid ""
"These are common features described in <xref linkend=\"colors-common-"
"features\"/>."
msgstr ""

#: src/menus/colors/auto/stretch-contrast.xml:114(title)
msgid "<quote>Stretch Contrast</quote> Example"
msgstr "<quote>Stretch Contrast</quote> Example"

#: src/menus/colors/auto/stretch-contrast.xml:123(para)
msgid ""
"The layer and its Red, Green and Blue histograms before <quote>Stretch "
"Contrast</quote>."
msgstr ""
"The layer and its Red, Green and Blue histograms before <quote>Stretch "
"Contrast</quote>."

#: src/menus/colors/auto/stretch-contrast.xml:138(para)
msgid ""
"The layer and its Red and Green and Blue histograms after <quote>Stretch "
"Contrast</quote>. The pixel columns do not reach the right end of the "
"histogram (255) because of a few very bright pixels, unlike <quote>White "
"Balance</quote>."
msgstr ""
"The layer and its Red and Green and Blue histograms after <quote>Stretch "
"Contrast</quote>. The pixel columns do not reach the right end of the "
"histogram (255) because of a few very bright pixels, unlike <quote>White "
"Balance</quote>."

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: src/menus/colors/auto/stretch-contrast-hsv.xml:70(None)
msgid ""
"@@image: 'images/menus/colors/auto/stretch-hsv.png'; "
"md5=fb016a380256c6bbf3a302281a432904"
msgstr ""
"@@image: 'images/menus/colors/auto/stretch-hsv.png'; "
"md5=fb016a380256c6bbf3a302281a432904"

#: src/menus/colors/auto/stretch-contrast-hsv.xml:10(title)
#: src/menus/colors/auto/stretch-contrast-hsv.xml:17(primary)
msgid "Stretch HSV"
msgstr "Stretch HSV"

#: src/menus/colors/auto/stretch-contrast-hsv.xml:14(secondary)
msgid "Stretch colors in HSV space"
msgstr "Stretch colors in HSV space"

#: src/menus/colors/auto/stretch-contrast-hsv.xml:20(para)
#, fuzzy
msgid ""
"The <guimenuitem>Stretch Contrast HSV</guimenuitem> command does the same "
"thing as the <link linkend=\"gimp-filter-stretch-contrast\">Stretch "
"Contrast</link> command, except that it works in HSV color space, rather "
"than RGB color space, and it preserves the Hue. Thus, it independently "
"stretches the ranges of the Hue, Saturation and Value components of the "
"colors. Occasionally the results are good, often they are a bit odd. "
"<quote>Stretch Contrast HSV</quote> operates on layers from RGB and Indexed "
"images. If the image is Grayscale, the menu entry is insensitive and grayed "
"out."
msgstr ""
"The <guimenuitem>Stretch HSV</guimenuitem> command does the same thing as "
"the <link linkend=\"gimp-filter-stretch-contrast\">Stretch Contrast</link> "
"command, except that it works in HSV color space, rather than RGB color "
"space, and it preserves the Hue. Thus, it independently stretches the ranges "
"of the Hue, Saturation and Value components of the colors. Occasionally the "
"results are good, often they are a bit odd. <quote>Stretch HSV</quote> "
"operates on layers from RGB and Indexed images. If the image is Grayscale, "
"the menu entry is insensitive and grayed out."

#: src/menus/colors/auto/stretch-contrast-hsv.xml:36(para)
#, fuzzy
msgid ""
"You can access this command from the image menubar through "
"<menuchoice><guimenu>Colors</guimenu><guisubmenu>Auto</"
"guisubmenu><guimenuitem>Stretch Contrast HSV</guimenuitem></menuchoice>."
msgstr ""
"You can access this command from the image menubar through "
"<menuchoice><guimenu>Colors</guimenu><guisubmenu>Auto</"
"guisubmenu><guimenuitem>Stretch HSV</guimenuitem></menuchoice>."

#: src/menus/colors/auto/stretch-contrast-hsv.xml:49(title)
#, fuzzy
msgid "<quote>Stretch Contrast HSV</quote> example"
msgstr "<quote>Stretch Contrast</quote> Example"

#: src/menus/colors/auto/stretch-contrast-hsv.xml:58(para)
#, fuzzy
msgid ""
"The active layer and its Red, Green and Blue histograms before "
"<quote>Stretch Contrast HSV</quote>."
msgstr ""
"The active layer and its Red, Green and Blue histograms before "
"<quote>Stretch HSV</quote>."

#: src/menus/colors/auto/stretch-contrast-hsv.xml:73(para)
#, fuzzy
msgid ""
"The active layer and its Red, Green and Blue histograms after <quote>Stretch "
"Contrast HSV</quote>. Contrast, luminosity and hues are enhanced."
msgstr ""
"The active layer and its Red, Green and Blue histograms after <quote>Stretch "
"HSV</quote>. Contrast, luminosity and hues are enhanced."

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: src/menus/colors/auto/equalize.xml:69(None)
msgid ""
"@@image: 'images/menus/colors/auto/equalize.png'; "
"md5=2000746549b6f65af80e17d64ff69021"
msgstr ""
"@@image: 'images/menus/colors/auto/equalize.png'; "
"md5=2000746549b6f65af80e17d64ff69021"

#: src/menus/colors/auto/equalize.xml:9(title)
#: src/menus/colors/auto/equalize.xml:13(secondary)
#: src/menus/colors/auto/equalize.xml:16(primary)
msgid "Equalize"
msgstr "Equalize"

#: src/menus/colors/auto/equalize.xml:18(para)
msgid ""
"The <guimenuitem>Equalize</guimenuitem> command automatically adjusts the "
"brightness of colors across the active layer so that the histogram for the "
"Value channel is as nearly flat as possible, that is, so that each possible "
"brightness value appears at about the same number of pixels as every other "
"value. You can see this in the histograms in the example below, in that "
"pixel colors which occur frequently in the image are stretched further apart "
"than pixel colors which occur only rarely. The results of this command can "
"vary quite a bit. Sometimes <quote>Equalize</quote> works very well to "
"enhance the contrast in an image, bringing out details which were hard to "
"see before. Other times, the results look very bad. It is a very powerful "
"operation and it is worth trying to see if it will improve your image. It "
"works on layers from RGB and Grayscale images. If the image is Indexed, the "
"menu entry is insensitive and grayed out."
msgstr ""
"The <guimenuitem>Equalize</guimenuitem> command automatically adjusts the "
"brightness of colors across the active layer so that the histogram for the "
"Value channel is as nearly flat as possible, that is, so that each possible "
"brightness value appears at about the same number of pixels as every other "
"value. You can see this in the histograms in the example below, in that "
"pixel colors which occur frequently in the image are stretched further apart "
"than pixel colors which occur only rarely. The results of this command can "
"vary quite a bit. Sometimes <quote>Equalize</quote> works very well to "
"enhance the contrast in an image, bringing out details which were hard to "
"see before. Other times, the results look very bad. It is a very powerful "
"operation and it is worth trying to see if it will improve your image. It "
"works on layers from RGB and Grayscale images. If the image is Indexed, the "
"menu entry is insensitive and grayed out."

#: src/menus/colors/auto/equalize.xml:37(para)
msgid ""
"You can access this command from the image menubar through "
"<menuchoice><guimenu>Colors</guimenu><guisubmenu>Auto</"
"guisubmenu><guimenuitem>Equalize</guimenuitem></menuchoice>"
msgstr ""
"You can access this command from the image menubar through "
"<menuchoice><guimenu>Colors</guimenu><guisubmenu>Auto</"
"guisubmenu><guimenuitem>Equalize</guimenuitem></menuchoice>"

#: src/menus/colors/auto/equalize.xml:48(title)
msgid "<quote>Equalize</quote> example"
msgstr "<quote>Equalize</quote> example"

#: src/menus/colors/auto/equalize.xml:57(para)
msgid ""
"The active layer and its Red, Green, Blue histograms before <quote>Equalize</"
"quote>."
msgstr ""
"The active layer and its Red, Green, Blue histograms before <quote>Equalize</"
"quote>."

#: src/menus/colors/auto/equalize.xml:72(para)
msgid "The active layer and its Red, Green, Blue histograms after treatment."
msgstr "The active layer and its Red, Green, Blue histograms after treatment."

#: src/menus/colors/auto/equalize.xml:76(para)
#, fuzzy
msgid ""
"Histogram stretching creates gaps between pixel columns giving it a striped "
"look: colors that occur frequently are stretched."
msgstr ""
"Histogram stretching creates gaps between pixel columns giving it a striped "
"look."

#: src/menus/colors/auto/color-enhance.xml:6(title)
#: src/menus/colors/auto/color-enhance.xml:13(primary)
msgid "Color Enhance"
msgstr "Color Enhance"

#: src/menus/colors/auto/color-enhance.xml:10(secondary)
#, fuzzy
msgid "Color enhance"
msgstr "Color Enhance"

#: src/menus/colors/auto/color-enhance.xml:16(para)
msgid ""
"The <guimenuitem>Color Enhance</guimenuitem> command stretches the Chroma "
"range of the colors in the layer to the maximum possible, without altering "
"Hue and Lightness. It does this by converting the colors to <ulink "
"url=\"https://en.wikipedia.org/wiki/"
"CIELAB_color_space#CIEHLC_cylindrical_model\">CIE LCh space</ulink>, then "
"stretching the Chroma range to be as large as possible, and finally "
"converting the colors back to its native color space."
msgstr ""

#: src/menus/colors/auto/color-enhance.xml:26(para)
#: src/menus/colors/auto/color-enhance-legacy.xml:30(para)
msgid ""
"This command does not work on Grayscale images. If the image is Grayscale, "
"the menu entry is disabled."
msgstr ""

#: src/menus/colors/auto/color-enhance.xml:33(title)
#: src/menus/colors/auto/color-enhance-legacy.xml:37(title)
msgid "Activate the command"
msgstr "Activate the command"

#: src/menus/colors/auto/color-enhance.xml:36(para)
#, fuzzy
msgid ""
"You can access this command from the main menu through "
"<menuchoice><guimenu>Colors</guimenu><guisubmenu>Auto</"
"guisubmenu><guimenuitem>Color Enhance</guimenuitem></menuchoice>."
msgstr ""
"You can access this command from the image menubar through "
"<menuchoice><guimenu>Colors</guimenu><guisubmenu>Auto</"
"guisubmenu><guimenuitem>Color Enhance</guimenuitem></menuchoice>."

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: src/menus/colors/auto/color-enhance-legacy.xml:76(None)
msgid ""
"@@image: 'images/menus/colors/auto/color-enhance.png'; "
"md5=2f419878c979c9438078646bd21d8f53"
msgstr ""
"@@image: 'images/menus/colors/auto/color-enhance.png'; "
"md5=2f419878c979c9438078646bd21d8f53"

#: src/menus/colors/auto/color-enhance-legacy.xml:9(title)
#: src/menus/colors/auto/color-enhance-legacy.xml:16(primary)
#, fuzzy
msgid "Color Enhance (legacy)"
msgstr "Color Enhance"

#: src/menus/colors/auto/color-enhance-legacy.xml:13(secondary)
#, fuzzy
msgid "Color enhance (legacy)"
msgstr "Color enhance"

#: src/menus/colors/auto/color-enhance-legacy.xml:19(para)
#, fuzzy
msgid ""
"The <guimenuitem>Color Enhance</guimenuitem> command increases the "
"saturation range of the colors in the layer, without altering brightness or "
"hue. It does this by converting the colors to HSV space, measuring the range "
"of saturation values across the image, then stretching this range to be as "
"large as possible, and finally converting the colors back to RGB. It is "
"similar to <link linkend=\"gimp-filter-stretch-contrast\">Stretch Contrast</"
"link>, except that it works in the HSV color space, so it preserves the hue."
msgstr ""
"The <guimenuitem>Color Enhance</guimenuitem> command increases the "
"saturation range of the colors in the layer, without altering brightness or "
"hue. It does this by converting the colors to HSV space, measuring the range "
"of saturation values across the image, then stretching this range to be as "
"large as possible, and finally converting the colors back to RGB. It is "
"similar to <link linkend=\"gimp-filter-stretch-contrast\">Stretch Contrast</"
"link>, except that it works in the HSV color space, so it preserves the hue. "
"It works on layers from RGB and Indexed images. If the image is Grayscale, "
"the menu entry is insensitive and grayed out."

#: src/menus/colors/auto/color-enhance-legacy.xml:40(para)
#, fuzzy
msgid ""
"You can access this command from the image menubar through "
"<menuchoice><guimenu>Colors</guimenu><guisubmenu>Auto</"
"guisubmenu><guimenuitem>Color Enhance (legacy)</guimenuitem></menuchoice>."
msgstr ""
"You can access this command from the image menubar through "
"<menuchoice><guimenu>Colors</guimenu><guisubmenu>Auto</"
"guisubmenu><guimenuitem>Color Enhance</guimenuitem></menuchoice>."

#: src/menus/colors/auto/color-enhance-legacy.xml:53(title)
#, fuzzy
msgid "<quote>Color Enhance (legacy)</quote> example"
msgstr "<quote>Color Enhance</quote> example"

#: src/menus/colors/auto/color-enhance-legacy.xml:62(para)
msgid ""
"The active layer and its Red, Green and Blue histograms before <quote>Color "
"Enhance</quote>."
msgstr ""
"The active layer and its Red, Green and Blue histograms before <quote>Color "
"Enhance</quote>."

#: src/menus/colors/auto/color-enhance-legacy.xml:70(title)
msgid "Command applied"
msgstr ""

#: src/menus/colors/auto/color-enhance-legacy.xml:79(para)
#, fuzzy
msgid ""
"The active layer and its Red, Green and Blue histograms after <quote>Color "
"Enhance (legacy)</quote>. The result may not always be what you expect."
msgstr ""
"The active layer and its Red, Green and Blue histograms after <quote>Color "
"Enhance</quote>. The result may not always be what you expect."

#. Put one translator per line, in the form of NAME <EMAIL>, YEAR1, YEAR2
#: src/menus/colors/auto/color-enhance-legacy.xml:0(None)
msgid "translator-credits"
msgstr "translator-credits"

#~ msgid ""
#~ "<guimenuitem>White Balance</guimenuitem> operates on layers from RGB "
#~ "images. If the image is Indexed or Grayscale, the menu item is "
#~ "insensitive and grayed out."
#~ msgstr ""
#~ "<guimenuitem>White Balance</guimenuitem> operates on layers from RGB "
#~ "images. If the image is Indexed or Grayscale, the menu item is "
#~ "insensitive and grayed out."

#~ msgid ""
#~ "Histogram stretching creates gaps between the pixel columns, giving it a "
#~ "stripped look."
#~ msgstr ""
#~ "Histogram stretching creates gaps between the pixel columns, giving it a "
#~ "stripped look."

#~ msgid ""
#~ "or by using the keyboard shortcut <keycombo><keycap>Shift</"
#~ "keycap><keycap>Page_Down</keycap></keycombo>."
#~ msgstr ""
#~ "or by using the keyboard shortcut <keycombo><keycap>Shift</"
#~ "keycap><keycap>Page_Down</keycap></keycombo>."

#~ msgid "<quote>Stretch HSV</quote> example"
#~ msgstr "<quote>Stretch HSV</quote> example"

#~ msgid ""
#~ "@@image: 'images/menus/colors/auto/normalize.png'; "
#~ "md5=4465af0091f3e4475ac0ee38959197cb"
#~ msgstr ""
#~ "@@image: 'images/menus/colors/auto/normalize.png'; "
#~ "md5=4465af0091f3e4475ac0ee38959197cb"

#~ msgid "Normalize"
#~ msgstr "Normalize"

#~ msgid ""
#~ "The <guimenuitem>Normalize</guimenuitem> command scales the brightness "
#~ "values of the active layer so that the darkest point becomes black and "
#~ "the brightest point becomes as bright as possible, without altering its "
#~ "hue. This is often a <quote>magic fix</quote> for images that are dim or "
#~ "washed out. <quote>Normalize</quote> works on layers from RGB, Grayscale, "
#~ "and Indexed images."
#~ msgstr ""
#~ "The <guimenuitem>Normalize</guimenuitem> command scales the brightness "
#~ "values of the active layer so that the darkest point becomes black and "
#~ "the brightest point becomes as bright as possible, without altering its "
#~ "hue. This is often a <quote>magic fix</quote> for images that are dim or "
#~ "washed out. <quote>Normalize</quote> works on layers from RGB, Grayscale, "
#~ "and Indexed images."

#~ msgid ""
#~ "You can access this command from the image menu bar through "
#~ "<menuchoice><guimenu>Colors</guimenu><guisubmenu>Auto</"
#~ "guisubmenu><guimenuitem>Normalize</guimenuitem></menuchoice>."
#~ msgstr ""
#~ "You can access this command from the image menu bar through "
#~ "<menuchoice><guimenu>Colors</guimenu><guisubmenu>Auto</"
#~ "guisubmenu><guimenuitem>Normalize</guimenuitem></menuchoice>."

#~ msgid "<quote>Normalize</quote>Example"
#~ msgstr "<quote>Normalize</quote>Example"

#~ msgid ""
#~ "The active layer and its Red, Green and Blue histograms before "
#~ "<quote>Normalize</quote>."
#~ msgstr ""
#~ "The active layer and its Red, Green and Blue histograms before "
#~ "<quote>Normalize</quote>."

#~ msgid ""
#~ "The active layer and its Red, Green and Blue histograms after "
#~ "<quote>Normalize</quote>. The contrast is enhanced."
#~ msgstr ""
#~ "The active layer and its Red, Green and Blue histograms after "
#~ "<quote>Normalize</quote>. The contrast is enhanced."

#~ msgid "<quote>Color Enhance</quote> example (Original image)"
#~ msgstr "<quote>Color Enhance</quote> example (Original image)"

#~ msgid "<quote>Color Enhance</quote> example (Image after the command)"
#~ msgstr "<quote>Color Enhance</quote> example (Image after the command)"
